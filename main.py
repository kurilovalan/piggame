from typing import Any
import random
 


class Player:
    points = 0
    def get_points(self):
        return  self.points 
    
    def set_points(self, value):
        self.points = value

    def add_points(self, value):
        self.points += value   

def max_score(players):
    max = 0
    k = 0
    maxk = 0
    for p in players:
        k += 1
        if p.get_points() > max:
            maxk = k
            max = p.get_points()
            
    return [max, maxk]



n = int(input("How many players will play? "))
players = []
for i in range(n):
    a = Player()
    players.append(a)

while max_score(players)[0] < 11:
    for i in range(len(players)):
        scoreTurn = 0
        print("player", i+1, "started turn",)
        b = "y"
        while b == "y":
            b = str(input("Would you like to roll? (y or n) "))
            if b == "y":
                points = random.randint(1, 6)
                if(points == 1):
                    print("Oops you have 1")
                    scoreTurn = 0
                    break
                else:                
                    print("you rolled: ", points)
                    scoreTurn += points 
                    print("you have: ", scoreTurn)
            if b == "n":
                players[i].add_points(scoreTurn)
                print("your total score is: ", players[i].get_points())


print("player ", max_score(players)[1], " is winner")